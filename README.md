# ID 生成器

**ID generator**
#

为了分布化和简单，采用了无状态的设计，参考了 twitter 的snowflake 算法，可以在负载均衡下实现了分布式id生成器
普通 PC 单节点每秒可以生成 33w ~ 35w 个 ID

功能点：

*  machineID 为了便于日后进行分布式扩展不通的服务器生成的 ID 不会冲突,支持初始化时通过参数下发，
默认采用 server 的 mac

*  自增 ID 选取时间的整数秒，一来相对浮点数加快了处理速度; 二来由于 sequenceID 的作用每个单位 ID 生
成的会更加均匀; 三来如果服务挂掉新生成的 ID 不会和历史 ID 产生冲突

*  增加了校验位，出于安全考虑

注：
mac 支持 windows 和 linux，获取的方式暂时不提供源码

典型用法：


```
#!python

import uuid
a = ID_generator()
id1 = a.gen()
```