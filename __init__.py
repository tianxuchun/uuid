"""
id 生成器
id generator

为了分布化和简单，采用了无状态的设计，参考了 twitter 的snowflake 算法，可以在负载均衡下实现了分布式id生成器
普通 PC 单节点每秒可以生成33~35w个id

功能点：
1. machineID 为了便于日后进行分布式扩展不通的服务器生成的id不会冲突,支持初始化时通过参数下发，
默认采用server的mac
2. 自增ID选取时间的整数秒，一来相对浮点数加快了处理速度; 二来由于 sequenceID 的作用每个单位id生
成的会更加均匀; 三来如果服务挂掉新生成的id不会和历史id产生冲突
3. 增加了校验位，出于安全考虑

注：
mac 支持 windows 和 linux，获取的方式暂时不提供源码

典型用法：

import uuid
a = ID_generator()
id1 = a.gen()
"""
from getnode import mac
from time import time
from md5 import md5
## mac address default
machineID = mac()

class ID_generator(object):
    sequenceID = 1
    timeID = hex(int(time()) & 0xffffffff)[-8:]
    def __init__(self,selfID = machineID):
        if selfID != machineID:
            hashkey = md5(selfID)
            standard_key = hashkey.hexdigest()[-12:]
        else:
            standard_key = selfID
        self.selfID = standard_key
## init sequenceID every second 
    def roll_seq(self,timeID):
        if timeID != self.timeID:
            self.sequenceID = 1   
            self.timeID = timeID
    def gen(self):
        timeID = hex(int(time()) & 0xffffffff)[-8:]
## generate ratio exceed limit,one node generate speed is between 33w/s and 35w/s
        if self.sequenceID == 0x1ffffff and timeID == self.timeID:
            print "?"
            return null
        self.sequenceID += 1
## | 0x1000000 for fix ID generate length
        sequenceID = hex(self.sequenceID | 0x1000000)[-7:]
        checksum = ord(sequenceID[-1]) + ord(self.selfID[-1]) + ord(timeID[-1]) & 0xf
        checkbit = hex(checksum)[-1]
        self.roll_seq(timeID)
        return sequenceID + self.selfID + timeID + checkbit
## test case
if __name__ == 'main':
    item = ID_generator()
    a = item.gen()
    checkbit = hex(ord(a[6])+ord(a[18])+ord(a[26])&0xf)[-1]
    if a[-1] == checkbit:
        print 'our ID'
